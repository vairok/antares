<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
	//rutas de los archivos para los modelos
	Route::resource('files','FilesController');

	Route::resource('commodity','CommoditiesController');
	Route::resource('menu','MenuController');
	Route::get('cost/{menu_id}',  'MenuController@cost')->name('cost');
	Route::put('menu/updatecost/{menu}', 'MenuController@updatecost')->name('updatecost');
    Route::get('destroyMe/{menu_id}', ['uses' => 'MenuController@destroyMe', 'as' => 'delete-menu']);
});

