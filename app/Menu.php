<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table    = "menu";
    protected $fillable = ['id', 'name', 'info', 'price', 'manufacturing_cost', 'cost_effectiveness', 'type_id'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
    public function commodities()
    {
        return $this->belongsToMany('App\Commodity', 'commodity_menu')->withPivot('quantity');
    }

}
