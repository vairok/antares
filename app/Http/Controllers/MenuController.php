<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Menu;
Use App\Type;
use Laracasts\Flash\Flash;
Use App\Commodity;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('id', 'asc')->get();
        
        foreach ($menus as $menu) {
            $costos=0;
            /*$commodities=Commodity::orderBy('id', 'desc')->wherePivot('menu_id', 10);
            ->wherePivot('approved', 1);*/
            foreach ($menu->commodities as $commodity) {
                $costos=($costos+($commodity->cost*$commodity->pivot->quantity));
            }
            $gan = $menu->price - $costos;
            $costos=(number_format($costos, 2, '.', ','));
            $rent = (($gan*100)/$menu->price);
            $rent=(number_format($rent, 1, '.', ','));
            $menu->update(['manufacturing_cost' => $costos]);
            $menu->update(['cost_effectiveness' => $rent]);

        }
        return view('menu.index', compact('menus'));
    }

    public function create()
    {
        $menu = Menu::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $type = Type::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        return view('menu.create', compact('menu', 'type'));
    }

    public function store(Request $request)
    {
        $request = $request->all();
        $menu = new Menu($request);
        $menu->save();
        flash('El producto '. $menu->name. ' ha sido agregado a la carta con exito!!', 'success')->important();
        return redirect()->route('cost', $menu->id);
    }

    public function show($id)
    {
        $menu = Menu::find($id);
        return view('menu.show', compact('menu'));
    }

    public function edit($id)
    {
        $menu = Menu::find($id);
        $type = Type::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        return view('menu.edit', compact('menu', 'type'));
    }

    public function update(Request $request, $id)
    {
        $request = $request->all();
        $menu = menu::find($id);
        $menu->update($request);
        flash('El producto '. $menu->name. ' ha sido editado en la carta con exito!!', 'success')->important();
        return redirect()->route('cost', $menu->id);
    }

    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();
        return $menu;
    }
    public function destroyMe($id)
    {
          $menu = self::destroy($id);
          flash('El producto '. $menu->name.' ha sido eliminado con exito!!', 'danger')->important();
          return redirect()->route('menu.index');
    }
    

    public function cost($id)
    {
        $menu = Menu::find($id);
        $products = Commodity::orderBy('id', 'DESC')->get();

        $precio = $menu->price;

        $aux = Commodity::orderBy('name', 'ASC')->get();
        $prod = $aux->toJson();
        $type = Type::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        flash('calculo de rentabilidad y costo de fabricacion de  '. $menu->name.'', 'warning')->important();
        return view('menu.cost', compact('menu', 'type', 'products', 'prod', 'precio'));
    }

    public function updatecost(Request $request, $id)
    {
/*
        $this->validate($request, [
            //''      => 'max:100|required',
            'product_id'   => 'required',
            //'locale'     => 'required|max:200',
        ]);*/

        $menu = Menu::find($id);

        if ($menu->commodities()->count() > 0) {
            $menu->commodities()->detach();
        }

        for ($i = 0; $i < count($request->product_id); $i++) {
            $menu->commodities()->attach($request->product_id[$i], ['quantity' => $request->quantity[$i]]);
        }

        $menu->manufacturing_cost = $request->get('costo_total');
        $menu->cost_effectiveness = $request->get('rent');/*
        $menu->date = $request->get('date');
        $menu->user_id = $request->get('user_id');
        $menu->event_id = $request->get('event_id');
        $order->iva = $request->get('iva');
        $order->total = $request->get('total');
        $order->locale = $request->get('locale');
        $order->discount = $request->get('discount');
        $order->notes = $request->get('notes');
        $order->neto = $request->get('neto');
        $order->updated = Auth()->user()->id*/
        $menu->save();

        Flash::success('Se ha registrado la orden de manera exitosa!')->important();
        return redirect()->route('menu.index');
    }
}
