<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
Use App\Unit;
Use App\Item;
Use App\Commodity;
class CommoditiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commodities = Commodity::orderBy('name', 'DESC')->get();
        return view('commodity.index', compact('commodities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $item = Item::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        return view('commodity.create', compact('units', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $request->all();
        $commodity = new Commodity($request);
        $commodity->save();
        flash('El producto '. $commodity->name. ' ha sido creado con exito!!', 'success')->important();
        return redirect()->route('commodity.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commodity = Commodity::find($id);
        $units = Unit::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $item = Item::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        return view('commodity.edit', compact('commodity', 'units', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request = $request->all();
        $commodity = Commodity::find($id);
        $commodity->update($request);
        flash('El producto '. $commodity->name. ' ha sido editado con exito!!', 'success')->important();
        return redirect()->route('commodity.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commodity = Commodity::find($id);
        $commodity->delete();
        flash('El producto '. $commodity->name.' ha sido eliminado con exito!!', 'danger')->important();
        return redirect()->route('commodity.index');
    }
}
