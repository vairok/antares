<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    protected $table    = "commodity";
    protected $fillable = ['id', 'name', 'cost', 'info', 'disp', 'min', 'max', 'unit_id', 'item_id'];

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function menus()
    {
        return $this->belongsToMany('App\Menu', 'commodity_menu')->withPivot('quantity');
    }
}
