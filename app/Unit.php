<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table    = "units";
    protected $fillable = ['id', 'name'];

    public function commodities()
    {
        return $this->hasMany('App\Commodity');
    }
}
