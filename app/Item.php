<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table    = "item";
    protected $fillable = ['id', 'name'];

    public function commodities()
    {
        return $this->hasMany('App\Commodity');
    }
}
