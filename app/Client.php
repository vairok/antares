<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table    = "clients";
    protected $fillable = ['id', 'name', 'email', 'type', 'monthly'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
