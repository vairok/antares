<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([//aquí inserto en la tabla usuarios la siguiente información:
            'name' => 'Antares-caballito',
            'email' => 'antarescaballito@gmail.com',
            'type' => 'restaurant',
            ]);
    }
}
