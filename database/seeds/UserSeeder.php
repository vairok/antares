<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'name' => 'Marcelo',
            'last_name' => 'Marcelo',
            'identification' => '12122323',
            'last_name' => 'Marcelo',
            'email' => 'marcelo@gmail.com',
            'password' => bcrypt('secret'),
            'type' => 'superuser',
            'client_id' => '1'
            ]);
    }
}
