<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommodityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commodity', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('cost');
            $table->text('info')->nullable();
            $table->string('disp')->nullable();
            $table->string('min')->nullable();
            $table->string('max')->nullable();
            $table->integer('unit_id')->unsigned(); 
            $table->integer('item_id')->nullable()->unsigned();    

            $table->foreign('item_id')->references('id')->on('item')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commodity');
    }
}