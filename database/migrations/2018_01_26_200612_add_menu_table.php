<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('info')->nullable();
            $table->string('price');
            $table->string('manufacturing_cost')->nullable();
            $table->string('cost_effectiveness')->nullable();
            $table->string('image')->nullable();
            $table->integer('type_id')->unsigned();
      
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('commodity_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('commodity_id')->unsigned();
            $table->string('quantity')->nullable();
            $table->timestamps();
            
            $table->foreign('menu_id')->references('id')->on('menu')->onDelete('cascade');
            $table->foreign('commodity_id')->references('id')->on('commodity')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commodity_menu');
        Schema::dropIfExists('menu');
    }
}
