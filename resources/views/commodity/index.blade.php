@extends('layouts.admin')

@section('title', 'Listado de mercaderia')

@section('contenido')
<div class="box">
    @include('partials.errors')
    <div class="box-header with-border">
        <h3 class="box-title">
            Listado de mercaderia
        </h3>
        <div class="box-tools">
            <div class="text-center">
                <a class="btn btn-primary btn-sm" href="{{ route('commodity.create') }}">
                    NUEVO MERCADERIA
                </a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12">

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover display table-responsive table-condensed" id="table">
                        <thead>
                            <tr>
                                <th>NOMBRE</th>
                                <th>UNIDAD</th>
                                <th>COSTO POR UNIDAD</th>
                                <th>DISPONIBLE</th>
                                <th>STOCK MINIMO</th>
                                <th>STOCK MAXIMO</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($commodities as $commodity)
                            <tr>
                                <td>
                                    {{ $commodity->name }}
                                </td>
                                <td>
                                    {{ $commodity->unit->name }}
                                </td>
                                <td>
                                    $ {{ $commodity->cost }}
                                </td>
                                <td>
                                    {{ $commodity->disp }}
                                </td>
                                <td>
                                    {{ $commodity->min }}
                                </td>
                                <td>
                                    {{ $commodity->max }}
                                </td>
                                <td>

                                    <a href="#" onclick="AbrirModal({{$commodity->id}})">
                                        <i class="glyphicon glyphicon-pencil openBtn" aria-hidden="true" ></i>
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(['route' => ['commodity.destroy', $commodity->id], 'method' => 'DELETE']) !!}
                                    <button class="glyphicon glyphicon-remove" onclick="return confirm('¿Realmente deseas borrar el producto?')">
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                    </div>
                </div>

                <!--  aqui va el modal -->

                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->

            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <!-- footer-->
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#table').DataTable({
            "language": {
                "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
            }
        });
    });

    function AbrirModal(id){
        var url = '{{ route("commodity.edit", ":id") }}';
        url = url.replace(':id', id);


        $('.modal-body').load(url,function(){
            $('#myModal').modal({show:true});
        });
    }
</script>

@endsection