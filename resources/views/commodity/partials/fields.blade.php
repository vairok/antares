
                                <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de mercaderia']) !!}
                                        </div> 
                                        <div class="form-group">
                                            {!! Form::label('unit_id', 'Unidad') !!}
                                            {!! Form::select('unit_id', $units, null, ['class' => 'form-control', 'placeholder' => 'Seleccione unidad']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('cost', 'Costo') !!}
                                            {!! Form::text('cost', null, ['class' => 'form-control', 'placeholder' => 'ingrese valor de compra']) !!}
                                        </div> 
                                        <div class="form-group">
                                            {!! Form::label('item_id', 'Rubro') !!}
                                            {!! Form::select('item_id', $item, null, ['class' => 'form-control', 'placeholder' => 'Seleccione rubro al que pertenece']) !!}
                                        </div>
                                        <div class="form-group">
                                          {!!Form::label('info', 'Información') !!}
                                         <br>
                                         {{ Form::textarea('info') }}
                                        </div> 
                                        <div class="form-group">
                                            {!! Form::label('disp', 'Cantidad disponible') !!}
                                            {!! Form::text('disp', null, ['class' => 'form-control', 'placeholder' => 'Ingrese cantidad']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('min', 'Stock minimo') !!}
                                            {!! Form::text('min', null, ['class' => 'form-control', 'placeholder' => 'ingrese minimo en stock']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('max', 'Stock maximo') !!}
                                            {!! Form::text('max', null, ['class' => 'form-control', 'placeholder' => 'ingrese maximo en stock']) !!}
                                        </div>
                                    <div class="for text-center">
                                        {!! Form::submit('REGISTRAR', ['class'=> 'btn btn-primary  btn-sm']) !!}
                                        <a class="btn btn-success btn-sm" href="{{url()->previous()}}">
                                            CANCELAR
                                        </a>
                                    </div>
                                </div>
          
<script type="text/javascript">
    CKEDITOR.replace( 'info' );
</script>
          