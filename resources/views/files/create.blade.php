@extends('layouts.admin')

@section('title', 'Registro de Archivos')

@section('contenido')

        @include('partials.errors')
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="container">
                        {!! Form::open(['route' => 'files.store', 'enctype'=> 'multipart/form-data']) !!}
                        {{ csrf_field() }}
                            @include('files.partials.fields')
                        {!! Form::close() !!}
                        <!-- /.box-body -->
                    </div>
                </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <!-- footer-->
        </div>
        <!-- /.box-footer-->

    <!-- /.box -->
            </div>
        </div>
@endsection

@section('js')
    <script type="text/javascript">
$(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });

            //plugin trumbowig
         //   $('.textarea-content').trumbowyg();

            //Editor de textos CKEDITOR
            CKEDITOR.replace( 'version2' );

    </script>
@endsection