                        <div class="col-md-11">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Registro de documento</h3>
                                </div>
                                <div class="col-md-10">
                                        <div class="form-group">
                                            {!! Form::label('title', 'Titulo') !!}
                                            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Ingrese titulo del documento']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('info', 'Informacion') !!}
                                            {!! Form::text('info', null, ['class' => 'form-control', 'placeholder' => 'ingrese info del documento']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!!Form::label('version1', 'Version 1') !!}
                                            {!!Form::file('version1')!!}
                                            <!--CREAMOS UN input tipo file para agregar archivos -->
                                        </div>
                                        <div class="form-group">
                                        {!!Form::label('version2', 'Version 2') !!}
                                            <br>
                                        <textarea name="version2" id="version2" rows="10" cols="80">
                                            
                                        </textarea>                                            
                                        </div>
                                    <div class="for text-center">
                                        {!! Form::submit('REGISTRAR', ['class'=> 'btn btn-primary  btn-sm']) !!}
                                        <a class="btn btn-success btn-sm" href="{{route('home')}}">
                                            CANCELAR
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>