
                                <div class="col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre de producto de la carta']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('type_id', 'Tipo de producto') !!}
                                            {!! Form::select('type_id', $type, null, ['class' => 'form-control', 'placeholder' => 'Seleccione tipo']) !!}
                                        </div>
                                        <div class="form-group">
                                            
                                         {!!Form::label('info', 'Información') !!}
                                         <br>
                                         {{ Form::textarea('info') }}
                                      
                                           
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('price', 'Precio') !!}
                                            {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'precio de carta']) !!}
                                        </div>
                                    <div class="for text-center">
                                        {!! Form::submit('REGISTRAR', ['class'=> 'btn btn-primary  btn-sm']) !!}
                                        <a class="btn btn-success btn-sm" href="{{route('home')}}">
                                            CANCELAR
                                        </a>
                                    </div>
                                </div>


<script type="text/javascript">

    CKEDITOR.replace( 'info' );
</script>
              