    <div class="box-bodys">
        <div class="row">
                        <!-- /.box-header -->
<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            @include('menu.partials.data-fields')
            <div class="col-md-6">
                
            </div>
        </div>
<div class="col-md-12">
    <div class="col-md-7">
                            <div class="box box-danger">
                                <div class="box-header">
                                    <h3 class="box-title">Calcular para obtener {{ $menu->name }}</h3>
                                        <div class="pull-right">
                                        </div>
                                </div>
                                    <div class="contacts">
                                        <div class="form-group multiple-form-group input-group">
                                            <div class="col-md-6">
                                                <label>Mercaderia utilizada</label>
                                                <div class="input-group-btn input-group-select">
                                        <div class="form-group">
                                            <select class="form-control select-product" name="product_id[]">
                                            <option selected="selected" disabled="disabled" hidden="hidden" value="">--Agregre mercaderia--
                                            </option>
                                            @foreach($products as $p)
                                                <option value="{{$p->id}}">{{$p->name}} / {{$p->unit->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                        </div>
                                        <input type="hidden" class="input-group-select-val" name="contacts['type'][]" value="phone">
                                    </div>
                                            </div>
                                            <div class="col-md-2">
                                    <label>Precio</label>
                                    {!! Form::text('product_cost[]', null, ['class' => 'form-control producto-price', 'placeholder' => 'precio', 'disabled' => 'true']) !!}
                              
                                </div>
                                            <div class="col-md-2">
                                                <label>Cantidad</label>
                                                {!! Form::text('quantity[]', null, ['class' => 'form-control producto-quantity', 'placeholder' => 'cantidad']) !!}
                                            </div>
                                            <div class="col-md-1">
                                                <label> - </label>
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-danger btn-remove">-</button>
                                                </span>
                                            </div>
                                            <div class="col-md-1">
                                                <label> - </label>
                                                <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-add">+</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-md-12">
                                                <span class="">
                                                    <button type="button" class="btn btn-success btn-cal">Calcular
                                                    </button>
                                                </span>
                                    </div>
                                </div>
                            </div>
        <div class="col-md-4">
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            @include('partials.errors')
                            <h3 class="box-title">Re-calculos: </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                    <p class="text-right">
                        {!! Form::hidden('total_cost', null, ['class' => 'form-control costo_total', 'placeholder' => 'Costo']) !!}
                    </p>
                    <div class="form-group">
                        <label for="total_show" class="col-sm-4 control-label">Precio</label>
                        <div class="col-sm-8">
                        {!! Form::text('precio', null, ['class' => 'form-control precio', 'placeholder' => $menu->price.' $', 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('precio', null, ['class' => 'form-control precio', 'placeholder' => 'precio']) !!}
                    </div>
                    <div class="form-group">
                        <label for="total_show" class="col-sm-4 control-label">Costo total</label>
                        <div class="col-sm-8">
                        {!! Form::text('costo_total', null, ['class' => 'form-control costo_total', 'placeholder' => $menu->manufacturing_cost, 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('costo_total', null, ['class' => 'form-control costo_total', 'placeholder' => 'Neto']) !!}
                    </div>
                    <div class="form-group">
                        <label for="iva_show" class="col-sm-4 control-label">Rentabilidad</label>
                        <div class="col-sm-8">
                        {!! Form::text('rent', null, ['class' => 'form-control rent', 'placeholder' => $menu->cost_effectiveness, 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('rent', null, ['class' => 'form-control rent', 'placeholder' => 'Neto']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
    
</div>
<div class="for text-left">
                    <input type="submit" value="Registrar nuevo calculo" class="btn btn-success  btn" onclick="return confirm('¿Realmente deseas reemplazar los datos anteriores para este producto?')"">
                        <a class="btn btn-danger btn" href="{{route('menu.index')}}">
                            Cancelar
                        </a>
                    </div>
            </div></div>