<!-- left column -->
<div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Datos del producto</h3>
                <a href="{{ route('menu.edit', $menu->id) }}" class="pull-right" onclick="return confirm('Se perderan los datos introducidos ¿Desea continuar?')"">Editar</a>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
            <div class="form-group">
                    <label for="inputtype3" class="col-md-4 control-label">Nombre</label>
                    <div class="col-sm-8">
                    <input type="type" class="form-control"
                           placeholder="{{$menu->name }}" disabled>
                    </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-4 control-label">Tipo</label>

                <div class="col-sm-8">
                    <input type="hidden" value="{{$menu->id}}" name="user_id" id="user_id">
                    <input type="email" class="form-control"
                           placeholder="{{$menu->type->name}}" disabled>
                </div>

            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-4 control-label">Información</label>

                <div class="col-sm-8">
                    <input type="password" class="form-control"
                           placeholder="{{$menu->info}}" disabled>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer -->
    </div>
</div>

<div class="col-md-6">
</div>
<!-- Rigth column -->
<div class="col-md-5">

        </div>



        <div class="col-md-12">
    <div class="col-md-7">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Mercaderia guardada para obtener {{ $menu->name }}</h3>
                                        <div class="pull-right">
                                        </div>
                                </div>
                                    <div class="contacts">
                                        <table class="table table-hover display table-responsive table-condensed" id="table">
                            <thead>
                            <tr>
                                <th>CANTIDAD</th>
                                <th>UNIDAD</th>
                                <th>NOMBRE</th>
                                <th>COSTO/UND</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($menu->commodities as $pro)
                                <tr>
                                    <td>
                                        {{ $pro->pivot->quantity }}
                                    </td>
                                    <td>
                                        {{ $pro->unit->name }}
                                    </td>
                                    <td>
                                        {{ $pro->name }}
                                    </td>
                                    <td>
                                        $ {{ $pro->cost }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                                    </div>
                                   
                                </div>
                            </div>
        <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            @include('partials.errors')
                            <h3 class="box-title">Calculo guardado: </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                    <div class="form-group">
                        <label for="total_show" class="col-sm-4 control-label">Precio</label>
                        <div class="col-sm-8">
                        {!! Form::text('precio', null, ['class' => 'form-control', 'placeholder' => $menu->price.' $', 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('precio', null, ['class' => 'form-control', 'placeholder' => 'precio']) !!}
                    </div>
                    <div class="form-group">
                        <label for="total_show" class="col-sm-4 control-label">Costo total</label>
                        <div class="col-sm-8">
                        {!! Form::text('costo_total', null, ['class' => 'form-control', 'placeholder' => $menu->manufacturing_cost, 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('costo_total', null, ['class' => 'form-control', 'placeholder' => 'Neto']) !!}
                    </div>
                    <div class="form-group">
                        <label for="iva_show" class="col-sm-4 control-label">Rentabilidad</label>
                        <div class="col-sm-8">
                        {!! Form::text('rent', null, ['class' => 'form-control', 'placeholder' => $menu->cost_effectiveness, 'disabled' => 'true']) !!}
                        </div>
                        {!! Form::hidden('rent', null, ['class' => 'form-control', 'placeholder' => 'Neto']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>