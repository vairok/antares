

@include('partials.errors')
   
<div class="box-body">
    <div class="row">
        <div class="col-xs-12">
            <div class="container">
                {!! Form::model($menu, ['route' => ['menu.update', $menu], 'method' => 'PUT']) !!}
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Editar carta</h3>
                        </div>
                        @include('menu.partials.fields')
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <!-- footer-->
        </div>
        <!-- /.box-footer-->

        <!-- /.box -->
    </div>
</div>


@section('js')

<script type="text/javascript">
    $(document).ready(function () {
        $('#table').DataTable({
            "language": {
                "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
            }
        });
    });
    CKEDITOR.replace( 'info' );
</script>
@endsection