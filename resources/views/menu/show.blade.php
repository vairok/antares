@extends('layouts.admin')

@section('title', 'Ver producto')

@section('contenido')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                Detalles del Producto
            </h3>
            <div class="box-tools">
                <div class="text-center">
                    <a class="btn btn-success btn-sm" href="{{ route('menu.index') }}">
                        Volver
                    </a>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                <div class="box box-solid box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Datos de {{ $menu->name }}
                        </h3>
                    </div>
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td>Nombre:</td>
                            <td>{{ $menu->name }}</td>
                        </tr>
                        <tr>
                            <td>Informacion:</td>
                            <td>{{ $menu->info }}</td>
                        </tr>
                        <tr>
                            <td>Precio de carta:</td>
                            <td>{{ $menu->price }}</td>
                        </tr>
                        @if($menu->manufacturing_cost != null)
                        <tr>
                            <td>Costo de fabricación:</td>
                            <td>{{$menu->manufacturing_cost}}</td>
                        </tr>
                        <tr>
                            <td>Rentabilidad:</td>
                            <td>{{$menu->cost_effectiveness}}</td>
                        </tr>
                        @else
                        <tr>
                            <td>Costo de fabricación:</td>
                            <td>No calculada</td>
                        </tr>
                        <tr>
                            <td>Rentabilidad:</td>
                            <td>No calculada</td>
                        </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="for text-center">
            <a class="btn btn-success btn" href="{{ route('menu.edit', $menu->id) }}">
                Editar
            </a>
            <a class="btn btn-danger btn" href="{{ route('menu.index') }}">
                Volver
            </a>
        </div>
        <br><br>
        <div class="for text-center">
            <a class="btn btn-primary btn" href="{{ route('cost', $menu->id) }}">
                CALCULAR RENTABILIDAD
            </a>
        </div>
    </div>
@endsection