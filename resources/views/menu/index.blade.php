@extends('layouts.admin')

@section('title', 'Listado de carta')

@section('contenido')


    <div class="box">
        @include('partials.errors')
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de carta
            </h3>
            <div class="box-tools">
                <div class="text-center">
                    <a class="btn btn-primary btn-sm" href="{{ route('menu.create') }}">
                        NUEVO PRODUCTO DE LA CARTA
                    </a>
                </div>

            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12">

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover display table-responsive table-condensed" id="table">
                        <thead>
                            <tr>
                                <th>NOMBRE</th>
                                <th>PRECIO</th>
                                <th>INFORMACION</th>
                                <th>TIPO</th>
                                <th>COSTO DE FABRICACION</th>
                                <th>RENTABILIDAD</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($menus as $menu)

                            <tr>
                                <td>
                                    {{ $menu->name }}
                                </td>
                                <td>
                                    $ {{ $menu->price }}
                                </td>
                                <td>
                                    {{  strip_tags($menu->info) }}
                                </td>
                                <td>
                                    {{ $menu->type->name }}
                                </td>
                                <td>
                                    {{ $menu->manufacturing_cost }}
                                </td>
                                <td>

                                    @if($menu->cost_effectiveness <= 35)
                                                                       <span class="label label-danger">
                                    {{ $menu->cost_effectiveness }}%
                        </span>
                            @elseif($menu->cost_effectiveness <= 70)
                                                                   <span class="label label-primary">
                            {{ $menu->cost_effectiveness }}%
                        </span>
                    @else
                    <span class="label label-success">
                        {{ $menu->cost_effectiveness }}%
                    </span>
                    @endif

                    </td>
                <td>
                    <a href="{{ route('menu.show', $menu->id) }}">
                        <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
                    </a>

                </td>
                <td>   
                    <a href="#" onclick="AbrirModal({{$menu->id}})">
                        <i class="glyphicon glyphicon-pencil openBtn" aria-hidden="true" ></i>
                    </a></td>

                <td>
                    {!! Form::open(['route' => ['menu.destroy', $menu->id], 'method' => 'DELETE']) !!}

                    <a href="{{ route('delete-menu', ['menu_id' => $menu->id]) }}" >  <i class="glyphicon glyphicon-remove" aria-hidden="true" ></i></a>

                    {!! Form::close() !!}
                </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    <div class="text-center">
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->

</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
    <!-- footer-->
</div>
<!-- /.box-footer-->
</div>
<!-- /.box -->
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#table').DataTable({
            "language": {
                "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
            }
        });
    });

    function AbrirModal(id){
        var url = '{{ route("menu.edit", ":id") }}';
        url = url.replace(':id', id);


        $('.modal-body').load(url,function(){
            $('#myModal').modal({show:true});
        });
    }


</script>
@endsection