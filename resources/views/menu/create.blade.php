@extends('layouts.admin')

@section('title', 'Registro de carta')

@section('contenido')

@include('partials.errors')
<div class="box-body">
    <div class="row">
        <div class="col-xs-12">
            <div class="container">
                {!! Form::open(['route' => 'menu.store']) !!}
                <div class="col-md-6 col-md-offset-2">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Registro de carta</h3>
                        </div>
                        @include('menu.partials.fields')
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <!-- footer-->
        </div>
        <!-- /.box-footer-->

        <!-- /.box -->
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#table').DataTable({
            "language": {
                "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
            }
        });
    });
     CKEDITOR.replace( 'info' );
</script>
@endsection